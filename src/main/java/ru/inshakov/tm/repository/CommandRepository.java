package ru.inshakov.tm.repository;

import ru.inshakov.tm.constant.ArgumentConst;
import ru.inshakov.tm.constant.TerminalConst;
import ru.inshakov.tm.api.command.ICommandRepository;
import ru.inshakov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Developer info"
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Commands"
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "App version"
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "System info"
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Close App"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null, "Create new task"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null, "Show task list"
    );

    private static final Command SHOW_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_SHOW_TASK_BY_INDEX, null, "Show task by index"
    );

    private static final Command SHOW_TASK_BY_NAME = new Command(
            TerminalConst.CMD_SHOW_TASK_BY_NAME, null, "Show task by name"
    );

    private static final Command REMOVE_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_INDEX, null, "Remove task by index"
    );

    private static final Command REMOVE_TASK_BY_NAME = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_NAME, null, "Remove task by name"
    );

    private static final Command REMOVE_TASK_BY_ID = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_ID, null, "Remove task by id"
    );

    private static final Command SHOW_TASK_BY_ID = new Command(
            TerminalConst.CMD_SHOW_TASK_BY_ID, null, "Show task by id"
    );

    private static final Command UPDATE_TASK_BY_INDEX = new Command(
            TerminalConst.CMD_UPDATE_TASK_BY_INDEX, null, "Update task by index"
    );

    private static final Command UPDATE_TASK_BY_ID = new Command(
            TerminalConst.CMD_UPDATE_TASK_BY_ID, null, "Update task by id"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null, "Clear all tasks"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null, "Create new project"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null, "Show project list"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null, "Clear all projects"
    );

    private static final Command SHOW_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_SHOW_PROJECT_BY_INDEX, null, "Show project by index"
    );

    private static final Command SHOW_PROJECT_BY_NAME = new Command(
            TerminalConst.CMD_SHOW_PROJECT_BY_NAME, null, "Show project by name"
    );

    private static final Command REMOVE_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_INDEX, null, "Remove project by index"
    );

    private static final Command REMOVE_PROJECT_BY_NAME = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_NAME, null, "Remove project by name"
    );

    private static final Command REMOVE_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_ID, null, "Remove project by id"
    );

    private static final Command SHOW_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_SHOW_PROJECT_BY_ID, null, "Show project by id"
    );

    private static final Command UPDATE_PROJECT_BY_INDEX = new Command(
            TerminalConst.CMD_UPDATE_PROJECT_BY_INDEX, null, "Update project by index"
    );

    private static final Command UPDATE_PROJECT_BY_ID = new Command(
            TerminalConst.CMD_UPDATE_PROJECT_BY_ID, null, "Update project by id"
    );



    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            SHOW_TASK_BY_INDEX, SHOW_TASK_BY_NAME,SHOW_TASK_BY_ID,
            REMOVE_TASK_BY_INDEX, REMOVE_TASK_BY_NAME, REMOVE_TASK_BY_ID,
            UPDATE_TASK_BY_INDEX, UPDATE_TASK_BY_ID,
            SHOW_PROJECT_BY_INDEX, SHOW_PROJECT_BY_NAME,SHOW_PROJECT_BY_ID,
            REMOVE_PROJECT_BY_INDEX, REMOVE_PROJECT_BY_NAME, REMOVE_PROJECT_BY_ID,
            UPDATE_PROJECT_BY_INDEX, UPDATE_PROJECT_BY_ID,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
