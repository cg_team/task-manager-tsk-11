package ru.inshakov.tm.constant;

public interface TerminalConst {

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_HELP = "help";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_TASK_CREATE = "task-create";

    String CMD_TASK_CLEAR = "task-clear";

    String CMD_TASK_LIST = "task-list";

    String CMD_SHOW_TASK_BY_INDEX = "show-task-by-index";

    String CMD_SHOW_TASK_BY_NAME = "show-task-by-name";

    String CMD_REMOVE_TASK_BY_INDEX = "remove-task-by-index";

    String CMD_REMOVE_TASK_BY_NAME = "remove-task-by-name";

    String CMD_REMOVE_TASK_BY_ID = "remove-task-by-id";

    String CMD_SHOW_TASK_BY_ID = "show-task-by-id";

    String CMD_UPDATE_TASK_BY_INDEX = "update-task-by-index";

    String CMD_UPDATE_TASK_BY_ID = "update-task-by-id";

    String CMD_ARGUMENTS = "arguments";

    String CMD_COMMANDS = "commands";

    String CMD_PROJECT_CREATE = "project-create";

    String CMD_PROJECT_CLEAR = "project-clear";

    String CMD_PROJECT_LIST = "project-list";

    String CMD_SHOW_PROJECT_BY_INDEX = "show-project-by-index";

    String CMD_SHOW_PROJECT_BY_NAME = "show-project-by-name";

    String CMD_REMOVE_PROJECT_BY_INDEX = "remove-project-by-index";

    String CMD_REMOVE_PROJECT_BY_NAME = "remove-project-by-name";

    String CMD_REMOVE_PROJECT_BY_ID = "remove-project-by-id";

    String CMD_SHOW_PROJECT_BY_ID = "show-project-by-id";

    String CMD_UPDATE_PROJECT_BY_INDEX = "update-project-by-index";

    String CMD_UPDATE_PROJECT_BY_ID = "update-project-by-id";

}
