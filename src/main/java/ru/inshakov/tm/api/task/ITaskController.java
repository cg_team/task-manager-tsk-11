package ru.inshakov.tm.api.task;

import ru.inshakov.tm.model.Task;

public interface ITaskController {

    abstract void showList();

    abstract void create();

    void showTaskByIndex();

    void showTask(Task task);

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

    abstract void clear();

}
