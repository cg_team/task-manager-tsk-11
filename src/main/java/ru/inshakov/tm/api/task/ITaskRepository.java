package ru.inshakov.tm.api.task;

import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    abstract List<Task> findAll();

    abstract void add(Task task);

    abstract void remove(Task task);

    abstract void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);
}
