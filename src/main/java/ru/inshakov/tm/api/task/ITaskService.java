package ru.inshakov.tm.api.task;

import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    abstract List<Task> findAll();

    abstract Task add(String name, String description);

    abstract void add(Task task);

    abstract void remove(Task task);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    Task findOneById(String id);

    Task removeOneById(String id);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

    abstract void clear();

}
