package ru.inshakov.tm.api.project;

import ru.inshakov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

    Project findOneById(String id);

    Project removeOneById(String id);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project updateProjectById(String id, String name, String description);

    void clear();

}
