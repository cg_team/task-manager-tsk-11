package ru.inshakov.tm.api.project;

import ru.inshakov.tm.model.Project;

public interface IProjectController {

    void showList();

    void create();

    void showProjectByIndex();

    void showProject(Project project);

    void showProjectByName();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeProjectById();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void clear();

}
